<?php include 'header.inc.php'; ?>
<script src="publications.js"> </script>

<head>
     <link rel="icon" type="image/png" href="Images/footer/mini_icon.png" />
</head>

<title>Publications@EEHPC</title>
<div id="headerArea">
	<h1>Publications</h1>
</div>
<div id="publicationArea">
		<div id="pubBarArea">
			<div id="pubBarHider">
			<div id="publicationBar">
				<!-- For a new year copy here to: -->
				<div id="2014" onclick="setYear(this)" class="pubYearButOn">Recent</div>
				<!-- Here. Then change the id to the current year, and change the text to the current year then go into the publications.js -->
				<div id="2010" onclick="setYear(this)" class="pubYearBut">2010 - Prior</div>

			</div>
		</div>
		</div>
		<div id="publicationInfo">
			
			 <?php include 'publications/recent.html'; ?>
		</div>
	</div>
	<div id="patentArea">
		<!-- To add another patent copy from here to -->
	<!--	<h1>Patents</h1>
		<li>  
			T. Mohsenin, P. Urard and B. Baas,<br>
     		"Improved Split-Row Decoding of LDPC Codes", US patent pending, filed 2009.<br>
 			<br>    
		</li>
		<li> 
			Tinoosh Mohsenin, and Bevan Baas,<br>      
      		"10GBASE-T LDPC Encoder/Decoder Research Chip,"<br>
      		<i class="noList">Proposal submitted to STMicroelectronics, September 2008.  </i>
		</li> 
		<br>     
		<li> 
			Tinoosh Mohsenin, and Bevan Baas,<br>      
     		"Ultra Low Power 10GBASE-T LDPC Decoder Research Chip,"<br>
     		<i class="noList">Proposal submitted to STMicroelectronics, August 2009.  </i>
     	</li> -->
	</div>
<!-- For a new year copy this from here: -->
<div class="yearInfo" id="year2014"><?php include 'publications/recent.html'; ?></div>
<!-- to here. Then change the id to year(what year it is). After that change year2013.html to year(what year it is).html and make that html doc -->
<div class="yearInfo" id="year2010"><?php include 'publications/prior.html'; ?></div>
<?php include 'footer.inc.php' ?>
