<?php include 'header.inc.php'; ?>

<title>Links@EEHPC</title>

<div id="headerArea">
	<h1>Links and Resources</h1>
</div>

<div class="linkArea">
	<a target="_blank" href="https://github.com/UMBC-EEHPC" class="linkRef">EEHPC Github</a><!-- Change to the title of the link change href to the url of the link -->
	<div class="linkInfo">- Github link to our projects</div><!-- Chage to the information about the link -->
</div>

<!-- Copy from here: -->
<div class="linkArea">
	<a target="_blank" href="src/docs/tutorials/EEHPC_Guidelines.pdf" class="linkRef">EEHPC Guidelines</a><!-- Change to the title of the link change href to the url of the link -->
	<div class="linkInfo">- Guidelines of EEHPC Lab</div><!-- Chage to the information about the link -->
</div>
<!-- to here -->

<div class="linkArea">
	<a target="_blank" href="http://web.ece.ucdavis.edu/vcl/misc/conferences.html" class="linkRef">VLSI & Signal Processing Conferences</a>
	<div class="linkInfo">- List of VLSI, Signal Processing, etc. conferences, journals, and magazines (deadlines are not updated)</div>
</div>

<div class="linkArea">
        <a target="_blank" href="http://www.cse.chalmers.se/research/group/vlsi/conference" class="linkRef">VLSI Conferences and Workshops</a>
        <div class="linkInfo">- VLSI related conferences and workshops from  CHALMERS UNIVERSITY OF TECHNOLOGY AND UNIVERSITY OF GOTHENBURG</div>
</div>

<div class="linkArea">
	<a target="_blank" href="http://web.ece.ucdavis.edu/vcl/misc/unix.help.html" class="linkRef">UNIX Command Notes</a>
	<div class="linkInfo">- Basic Unix commands</div>
</div>

<div class="linkArea">
	<a target="_blank" href="http://heather.cs.ucdavis.edu/~matloff/unix.html" class="linkRef">UNIX Tutorials</a>
	<div class="linkInfo">- Tutorial on Unix/Linux</div>
</div>

<div class="linkArea">
	<a target="_blank" href="http://web.ece.ucdavis.edu/vcl/misc/latex.help.html" class="linkRef">Latex Notes</a>
	<div class="linkInfo">- Information on LaTeX document software</div>
</div>

<div class="linkArea">
	<a target="_blank" href="src/docs/tutorials/Synthesis And Layout.pdf" class="linkRef">Synthesis and Layout Tutorial</a>
	<div class="linkInfo">- Tutorial for synthesis and Encounter Place+Route</div>
</div>

<div class="linkArea">
	<a target="_blank" href="src/docs/tutorials/NCVerilog_Tutorial.pdf" class="linkRef">NCVerilog Tutorial</a>
	<div class="linkInfo">- Tutorial on simulation in Cadence NCVerilog</div>
</div>


<?php include 'footer.inc.php' ?>
