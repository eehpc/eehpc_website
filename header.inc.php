<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--<html xmlns="http://www.w3.org/1999/xhtml"> -->
<html>
<head>
	<base href="https://eehpc.csee.umbc.edu/" />
	<!-- <base href="http://localhost:8888/" /> -->
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
	<meta http-equiv="last-modified" content="Fri, 20 July 2012 13:36:57 EST" />
	<meta http-equiv="cache-control" content="max-age=2592000" />
	<meta http-equiv="content-language" content="en-US" />
	<meta http-equiv="Content-Encoding" content="gzip" />
	<meta http-equiv="Accept-Encoding" content="gzip, deflate" />

	<meta name="author" content="Nathan Page" />
	<meta name="description" content="Energy" />
	<meta name="keywords" content=" custom electronics, optical mechanical systems, full instruments, PCB layout, microcontrollers, research, circuit design, board design, test, inspection, embedded firmware, instrument software"/>

	<meta name="viewport" content="width=1000px, minimum-scale=0.1, maximum-scale=2.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	
	
	<!-- Favicon for every browser and Apple product-->
	<!--[if lt IE 9]>
		<link rel="shortcut icon" href="favicon.ico" />
	<![endif]-->
	<link rel="icon" type="image/png" href="favicon.png?v=2"/>
	<link rel="apple-touch-icon" href="apple-touch-icon.png" />
	<link rel="stylesheet" href="base.css?v=3" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" type="image/png" href="QR_code.png?v=2"/>
	

</head>
<body>
<?php 
//define('BASE_URL', 'https://eehpc.csee.umbc.edu/');
//define('BASE_URL', 'http://localhost:8888/');
include 'nav.inc.php';

?>

