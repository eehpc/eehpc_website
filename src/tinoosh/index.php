<?php include "header.inc.php" ?>


<style type="text/css">
	#sidebar{
		display: none;
	}
	.fullLine{
		margin-left: 0;
	}


</style>
<!-- <section id="slideshow">
	<div id="swipeArea">
		<div id='mySwipe' class='swipe'>
		  <div class='swipe-wrap'>
		    <div class="slide" style="background-image:url(http://placehold.it/530x320)">
		    </div>
		    <div class="slide" style="background-image:url(http://placehold.it/530x320)">
		    </div>
		    <div class="slide" style="background-image:url(http://placehold.it/530x320)">
		    </div>
		    <div class="slide" style="background-image:url(http://placehold.it/530x320)">
		    </div>
		  </div>
		  <div onclick='mySwipe.prev()' class="prevButton"></div>
		  <div onclick='mySwipe.next()' class="nextButton"></div>
		</div>
	</div>
</section> -->

<section id="genInfo">

	<div class="blockThird">
		<img class="profilePic" src="img/navigation/Tinoosh.png">
	</div>
	<div class="blockThird">
		<h2>Tinoosh Mohsenin <br><small>Assistant Professor</small></h2>
		<a class="offsiteLink" href="http://www.umbc.edu/">
			<img class="halfSizeImage" src="img/content/umbcLogo.png">
		</a>
		<a class="offsiteLink" href="http://eehpc.csee.umbc.edu/">
			<img class="halfSizeImage" src="img/content/eehpcLogo.png">
		</a>
	</div>
	<div class="blockThird">
		<h4>Office</h4>
		<p>
			Information Technology/ Engineering Building, ITE 323<br>
			University of Maryland, Baltimore County
		</p>

		<h4>Email</h4>
		<p>
			tinoosh@umbc.edu
		</p>	
		<h4>Phone</h4>
		<p>
			410-455-1349 <br>
			410-455-3969 (fax)
		</p>	
		<h4>Lab Website</h4>
		<p>
			<a href="http://eehpc.csee.umbc.edu">eehpc.csee.umbc.edu</a>
		</p>
	</div>	


</section>

<section class="fullLine">
	<p>
		In general, I am interested in high performance and energy-efficient VLSI computation that support communication, signal
		processing, error correction, and biomedical applications. These include:
	</p>
	 <ul>
		<li>Domain specific many-core platforms for wearable and communication platforms</li>
		<li>Low power DSP fnd machine learning algorithms and hardware for personalized health  monitoring</li>
		<li>
			Algorithm and architecture enhancements
			<ul>
				<li> Compressive sensing reconstructon</li>
				<li>Low density parity check codes (LDPC)</li>
			</ul>
		</li>
		<li>Application mapping/software development on many-core architectures</li>
		<li>VLSI design of ASICs and reconfigurable architectures </li>
		<li>
			Single-chip solutions targeted for low-power embedded systems through a co-design of
			programmable cores and application-specific processors.
		</li>
	</ul>
</section>
<!-- 
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src='js/libraries/swipe.js'></script>
<script>
// pure JS
var elem = document.getElementById('mySwipe');
var mySwipe = Swipe(elem, {
  	startSlide: 0,
  	auto: 3000,
  	continuous: true,
  	disableScroll: true,
  	stopPropagation: false,
  	callback: function(index, element) {
  		console.log(index);

  	},
  	transitionEnd: function(index, element) {}
});
</script>
 -->
        
<?php include "footer.inc.php" ?>

