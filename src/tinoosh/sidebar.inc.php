<section id="sidebar">
	<img id="profileImg" src="img/navigation/Tinoosh.png">
	<h2>Tinoosh Mohsenin</h2>
	<h4>Office</h4>
	<p>
		Information Technology/ Engineering Building, ITE 323<br>
		University of Maryland, Baltimore County
	</p>

	<h4>Email</h4>
	<p>
		tinoosh@umbc.edu
	</p>	
	<h4>Phone</h4>
	<p>
		410-455-1349 <br>
		410-455-3969 (fax)
	</p>	
	<!-- <h4>Lab Website</h4>
	<p>
		<a href="http://eehpc.csee.umbc.edu">eehpc.csee.umbc.edu</a>
	</p> -->
	<a class="offsiteLink" href="http://www.umbc.edu/">
		<img class="halfSizeImage" src="img/content/umbcLogo.png">
	</a>
	<a class="offsiteLink" href="http://eehpc.csee.umbc.edu/">
		<img class="halfSizeImage" src="img/content/eehpcLogo.png">
	</a>	
</section>