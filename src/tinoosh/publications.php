<?php include "header.inc.php" ?>

<!-- The php changes the href of the projRef to the correct path  -->
<section class="publications">
<?php

addPublications("publications/recent.html");
addPublications("publications/prior.html");
function addPublications($fileLocation){
	$doc = new DOMDocument();
	$doc->loadHTMLFile($fileLocation);
	$xpath = new DOMXPath($doc);

	$titleNodes = $doc->getElementsByTagName('b');

	foreach($titleNodes as $titleNode){
		$titleNode-> setAttribute("class","titleYear");
	}


	$publicationLinks = $doc->getElementsByTagName('a');

	foreach($publicationLinks as $publicationLink){
		$oldLink = $publicationLink -> getAttribute("href");
		//remove spaces
		$oldLink = trim($oldLink);
		//make sure the href is long enough to check
		if(strlen($oldLink)>6){
			$checkIfHttp = substr($oldLink, 0,4);
			//see if it is already an offsite link
			if(strcasecmp($checkIfHttp,"http") !==0){
				//make it an offsite link to eehpc
				$publicationLink ->setAttribute("href","http://eehpc.csee.umbc.edu/" . $oldLink);
			}
		}
		
	}
	echo $doc->saveHTML();
}
?>
</section>

<?php include "footer.inc.php" ?>