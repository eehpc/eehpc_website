<?php include "header.inc.php" ?>

<!-- The php changes the href of the projRef to the correct path  -->
<?php

$doc = new DOMDocument();
$doc->loadHTMLFile("research/research.html");
$xpath = new DOMXPath($doc);

$nodes = $xpath->query("//*[@class='projRef']");
foreach($nodes as $node){
	$oldHref=$node->getAttribute("href");
	$node->setAttribute("href","http://eehpc.csee.umbc.edu/".$oldHref);
}


$nodes = $xpath->query("//*[@class='projectImage']");
foreach($nodes as $node){ 
	$oldLink = $node -> getAttribute("src");
	//remove spaces
	$oldLink = trim($oldLink);
	//make sure the href is long enough to check
	if(strlen($oldLink)>6){
		$checkIfHttp = substr($oldLink, 0,4);
		//see if it is already an offsite link
		if(strcasecmp($checkIfHttp,"http") !==0){
			//make it an offsite link to eehpc
			$node ->setAttribute("src","http://eehpc.csee.umbc.edu/" . $oldLink);
		}
	}		
}
	
echo $doc->saveHTML();

?>


<?php include "footer.inc.php" ?>