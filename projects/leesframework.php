<?php include '../header.inc.php'; ?>

<title>EEHPC Lab</title>
<div id="headerArea">
	<img class="headerIcon" src="../src/images/research_icon/LESSIcon.PNG" width="150px">
	<h1 class="headerProject">Light Encryption using Scalable Sketching (LESS) for Big Data Acceleration</h1>
</div>

<img class="projectImg2" src="../src/images/research/LESSAdvantage.PNG" />


<p class="projectText">
FPGA has demonstrated speedup, and energy efficiency as compared to CPU, for MapReduce framework. However, size of data affects choice of tiny cores for efficient Big Data processing. In many Big Data analytics workloads approximate results suffice. In this project we propose Sketching based fraemwork called LESS: Light Encryption using Scalable Sketching to accelerate Big Data processing on hardware platforms. We tested LESS framework for Face Detection, Objects and Scenes identification, Text analysis and Bio-medical applications. Integration of LESS framework with Hadoop MapReduce Platform shows that, it achieves 46% reduction in data transfers with very low execution overhead of 0.11% and negligible energy overhead of 0.001% when tested for 50,000 number of image.
</p>

<img class="projectImg2" src="../src/images/research/LESSFramework.PNG" />


<br>
<div class="projLinkArea">
	<a href="publications.php" class="projLink">View Publications</a>
</div>

<?php include '../footer.inc.php';?>
