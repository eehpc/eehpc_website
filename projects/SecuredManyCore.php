<?php include '../header.inc.php'; ?>

<head>
     <link rel="icon" type="image/png" href="Images/footer/mini_icon.png" />
	 
	 <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<style>
	.carousel-indicators li {
		background-color: #000000;
	}
	.carousel-indicators .active {
	 	background-color: #000000;
	}
	.carousel-inner > .item > img,
	.carousel-inner > .item > a > img {
      height: 345px;
      margin: auto;
	  margin-bottom: 0px;
  }
  body{
    background-color: #fbfbfb;
  }
  #container{
    width: 1000px!important;
  }
  #myCarousel{
	height: 430px;
	width:1000px;
	margin-bottom:0px;
  }
  #pageArea{
    overflow:hidden;
  }
  </style>
</head>

<title>Research@EEHPC</title>
<div id="headerArea">
	<img class="headerIcon" src="../src/images/research_icon/cs_security_icon.png" width="150px"\>
	<h1 class="headerProject">Energy Efficient Secured Many-Core Processor for Embedded Applications</h1>
</div>

<p class="projectText">
<br>
Increased focus on R&amp;D and reduction in time-to-market window in most of the semiconductor companies a new trend has started to rely on Third Party Intellectual Properties and outsourcing fabrication process. This raises serious security concern about Hardware Trojan inclusions in recent years.
The goal of this research is to design a real-time Trojan detection framework for a custom Many-Core by using an effective method such that it has minimal hardware overhead and implementation complexity with a very high accuracy of detection. 
</p>

<div class="container">
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1" color="#000000"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="../src/images/research/SixteenTestSetup.png" alt="Chania" height="345">
      </div>
      <div class="item">
        <img src="../src/images/research/SecurityRes1.PNG" alt="Chania" height="345">
      </div>    
    </div>
  </div>
</div>

<br>
<div class="projLinkArea">
	<a href="publications.php" class="projLink">View Publications</a>
</div>


<?php include '../footer.inc.php';?>
