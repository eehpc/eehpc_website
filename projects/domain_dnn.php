<!-- To make a new projects page called '(product name).php' copy this whole php file-->
<?php include '../header.inc.php'; ?>
<head>
	<style>
	div.container {
		display:inline-block;
	}
	</style>

</head>
<!-- Change this to title of project -->
<title>EEHPC Lab</title>

<br>
<!--
<div class="projLinkArea">
	<a href="research.php" class="projLink">Back</a>
</div>
-->

<div id="headerArea">
	<img class="headerIcon" src="../src/images/research_icon/domain_dnn_icon.png"\ width="150px">
	<h1 class="headerProject"> Deep Neural Nets for Embedded Big Data Applications</h1>
</div>

<p class="projectText">
We explore the use of deep neural networks (DNN) for embedded big data applications. Deep neural networks have been demonstrated to outperform state-of-the-art solutions for a variety of complex classification tasks, such as image recognition. The ability to train networks to both perform feature abstraction and classification provides a number of key benefits. One key benefit is that it reduces the burden of the developer to produce efficient, optimal feature engineering, which typically requires expert domain-knowledge and significant time. A second key benefit is that the network's complexity can be adjusted to achieve desired accuracy performance. Despite these benefits, DNNs have yet to be fully realized in an embedded setting. In this research, we explore novel architecture optimizations and develop optimal static mappings for neural networks onto highly parallel, highly granular hardware processors such as CPUs, many-cores, embedded GPUs, FPGAs, and ASIC.
</p>

<img class="projectImg" src="../src/images/research/Morteza_FP_layers.png"/>

<p class="projectText">
Two of the most popular optimization methods for DNNs include quantization and sparsification:
</p>

<p class="projectText">
1)	In quantization, the DNN model is trained such that the precision of parameters and the intermediate data, aka feature map, is lowered and limited to certain number of bits, and it can be applied on both weights and feature map of a DNN, e.g. 1 or 2 bits as in binary or ternary neural networks (BBNs and TNNs). With quantization, the model size shrinks proportional to the precision level, and the high-complexity operations (e.g. float-point multiplication) can be simplified with simple operations using simple hardware circuitry (e.g. XNOR logic as a multiplier for 1-bit values in BNNs).
</p>

<img class="projectImg" src="../src/images/research/Morteza_T_layers.png"/>

<p class="projectText">
2)	In pruning methods, a DNN architecture in which zero values are omnipresent is pursued. Since the computation in DCNNs is dominated by multiply-accumulate (MAC) operations, everywhere that zero values exist the MAC operations can be skipped. This method is also referred to as fine-grained pruning, the main drawback of which is the irregular patterns of non-zero weights in the DNN model that necessitate having a compressing format and a decompression algorithm. Despite their efficacy, pruning methods are not suitable for extremely quantized DNNs.
</p>

<img class="projectImg" src="../src/images/research/Morteza_PT_layers.png"/>

<p class="projectText">
3)	Structurally sparsifying DNN layers, on the other hand, are shown to be on par with pruning methods. Contrary to pruning methods that follow a train-prune routine, structurally sparsified methods can be considered as a prune-train routine in which the DNN is forced to be trained on an already-sparsified basis. The advantage of structural sparsity is two folds: exempting from  indexing as required in pruning methods, and usability for extremely quantized DNNs.
</p>

<img class="projectImg" src="../src/images/research/Morteza_CSC_layers.png"/>

<img class="projectImg" src="../src/images/research/Morteza_results.png"/>

<!--
<p class = "projectText">
<br>	
We explore the use of deep neural networks (DNN) for embedded big data applications. Deep neural networks have been demonstrated to outperform state-of-the-art solutions for a variety of complex classification tasks, such as image recognition. The ability to train networks to both perform feature abstraction and classification provides a number of key benefits. One key benefit is that it reduces the burden of the developer to produce efficient, optimal feature engineering, which typically requires expert domain-knowledge and significant time. A second key benefit is that the network's complexity can be adjusted to achieve desired accuracy performance. Despite these benefits, DNNs have yet to be fully realized in an embedded setting. In this research, we explore novel architecture optimizations and develop optimal static mappings for neural networks onto highly parallel, highly granular hardware processors such as many-cores and embedded GPUs.
<br>	

</p>

<img class="seizureImg" src="../src/images/research/dnn_mapping.svg"/>

<img class="seizureImg" src="../src/images/research/dnn_loss_complexity.svg"/>


<div style="width:900px; margin:0 auto;">
	<div class="container">
	  <img src="../Images/research/dnn_mapping.svg"/ width="425" margin="0 0" />
	</div>
	<div class="container">
	  <img src="../Images/research/dnn_loss_complexity.svg"/ width="425" margin="0 0" />
	</div>
</div>
-->

<br>
<div class="projLinkArea">
	<a href="publications.php" class="projLink">View Publications</a>
</div>

<!-- Don't change anything under here -->
<?php include '../footer.inc.php';?>
