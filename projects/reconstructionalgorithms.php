<?php include '../header.inc.php'; ?>

<title>EEHPC Lab</title>
<div id="headerArea">
	<img class="headerIcon" src="../src/images/research_icon/cs_icon.svg" width="150px">
	<h1 class="headerProject">Efficient Compressive Sensing Reconstruction Algorithms and Architectures</h1>
</div>

<img class="projectImg2" src="../src/images/research/CSDiagram.png" />


<p class="projectText">
Compressive sensing (CS) is a novel technology that enables sampling sparse signals at sub-Nyquist rates and reconstruction of the signals using computational intensive algorithms. The reconstruction algorithms are complex and consume a considerable amount of power. In addition, software implementation of these algorithms is extremely slow. In this project, we propose reduced complexity of reconstruction algorithms and efficient hardware implementation of them for different platforms including FPGA, ASIC, GPU, and many-core platforms.
</p>

<p class="projectText">
On the other hand, direct use of compressively sensed data has been proposed recently which eliminates the need for applying reconstruction algorithms. The compressively sensed data can be used directly for classification purposes. In this space, we mainly work on compressive sensing sampler optimization in terms of complexity, power consumption and performance. 	
</p>


<img class="projectImg2" src="../src/images/research/CompressiveSensing.png" />


<br>
<div class="projLinkArea">
	<a href="publications.php" class="projLink">View Publications</a>
</div>

<?php include '../footer.inc.php';?>
