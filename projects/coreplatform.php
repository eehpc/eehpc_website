<!-- To make a new projects page called '(product name).php' copy this whole php file-->
<?php include '../header.inc.php'; ?>
<head>
     <link rel="icon" type="image/png" href="Images/footer/mini_icon.png" />
</head>
<!-- Change this to title of project -->
<title>EEHPC Lab</title>
<div id="headerArea">
	<!-- Change this to title of project -->
	<h1 class="headerProject">Many core platform implementation using 3D Technology</h1>
</div>
<!-- bring picture you have into the gallerys folder and set src value to "Images/gallery/(new image)" -->

<!-- Change text to info about the project. This whole 'p' area can be copy pasted for more paragraphs -->
<p class="projectText">
	Future embedded, high performance, and cloud computing must meet limited energy capacity, cost, and sustainability. 4G+ mobile communication devices will regularly
	execute over one tera-operations per second (TOPS) with a variety of diverse workloads---from baseband communications to augmented reality---while operating on a 5
	to 25~Watt-hour cellphone/tablet battery. Similarly, supercomputer clusters must achieve exaFLOP performance within 20~MWatt in order to remain feasible within 
	infrastructure constraints.
</p>

<p class="projectText">
	By utilizing upcoming 3D integrated circuit (3DIC) technologies, we can further increase the computational capacity of MPPAs towards 1 TOPS within a 1 Watt budget (1 pJ/op).
 	Such a platform will be two to three orders of magnitude more energy efficient than the current state-of-the-art superscalar and VLIW multi-core, GPGPU, or 2D MPPA 
	architectures. Our approach is to utilize the 3DIC paradigm to extend the 2D MPPA for rapid local runtime reconfiguration for dynamic or multi-application deployments. 
	A 3D die stack that consists of an MPPA, I/O, control, and memory layers can provide the base for scalable adaptive hardware that can diversify the types and number of 
	functions per area, while maintaining a high energy and area efficiency. 
</p>

<!-- Don't change anything under here -->


<?php include '../footer.inc.php';?>
