<!-- To make a new projects page called '(product name).php' copy this whole php file-->
<?php include '../header.inc.php'; ?>
<head>
     <link rel="icon" type="image/png" href="Images/footer/mini_icon.png" />
</head>
<!-- Change this to title of project -->
<title>EEHPC Lab</title>

<div class="projLinkArea">
	<a href="research.php" class="projLink">Back</a>
</div>

<div id="headerArea">
	<!-- Change this to title of project -->
	<h1 class="headerProject">Signal processing architectures for ambulatory seizure detection device</h1>
</div>
<!-- bring picture you have into the gallerys folder and set src value to "Images/gallery/(new image)" -->
<!-- Change text to info about the project. This whole 'p' area can be copy pasted for more paragraphs -->


<img class = "seizureImg" src="../Images/gallery/seizureDetectAlgDiagrams.bmp" />
<img class = "seizureImg" src="../Images/gallery/AdamSeizure.jpg" />
<p class = "projectText">
 The research will explore an approach to overcome problems of sensor noise, false detection, 
and energy/power constraints by combining the analysis of multiple physiological 
signals through specialized hardware implementing a multi-layer classification 
technique comprised of signal processing and machine learning functions. 
The hybrid architecture will leverage common operations and communication patterns 
between digital signal processing and machine learning to support these computations more 
efficiently than traditional approaches. The prototype system will be evaluated for use in 
wearable seizure detection devices by using traces of EEG and other physiological sensor data 
obtained from the Epilepsy Center at University of Maryland Medical Center.
</p>


<!-- Don't change anything under here -->
<?php include '../footer.inc.php';?>
