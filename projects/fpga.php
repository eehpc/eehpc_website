<?php include '../header.inc.php'; ?>
<head>
     <link rel="icon" type="image/png" href="Images/footer/mini_icon.png" />
</head>
<title>EEHPC Lab</title>
<div id="headerArea">
	<h1 class="headerProject">FPGA Implementation of a full parallel LDPC Decoder for 802.11ad WLAN</h1>
</div>
<p> Note: Insert new image of this item.</p>

<p class="projectText">
	This work presents an FPGA implementation of a full parallel single pipelined LDPC decoder for the 802.11ad WLAN standard. The 
		decoder uses a (672, 588) rate 7/8 LDPC code, with BPSK modulation over AWGN channel. 
</p>

<?php include '../footer.inc.php';?>
