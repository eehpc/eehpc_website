<?php include '../header.inc.php'; ?>
<head>
     <link rel="icon" type="image/png" href="Images/footer/mini_icon.png" />
</head>
<title>EEHPC Lab</title>
<div id="headerArea">
	<h1 class="headerProject">Massively Parallel clusters on chip for smart health care</h1>
</div>
<p> Note: Insert new image of this item.</p>

<p class="projectText">
	By utilizing upcoming 3D integrated circuit (3DIC) technologies, we can further increase the computational capacity of 
		 MPPAs towards 1 TOPS within a 1 Watt budget (1 pJ/op).
		 Such a platform will be two to three orders of magnitude more energy efficient than 
		 the current state-of-the-art superscalar and VLIW multi-core, GPGPU, or 2D MPPA architectures
</p>


<?php include '../footer.inc.php';?>
