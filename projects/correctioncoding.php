<?php include '../header.inc.php'; ?>
<head>
     <link rel="icon" type="image/png" href="Images/footer/mini_icon.png" />
</head>
<title>EEHPC Lab</title>
<div id="headerArea">
	<h1 class="headerProject">Energy Efficient Error Correction Coding</h1>
</div>
<p> Note: Insert new image of this item.</p>

<p class="projectText">	
	Communication systems are becoming a standard requirement of every computing platform from wireless sensors, mobile telephony, and server class
	computers. Local and cellular wireless communication throughputs are expected to increase to hundreds of Mbps and even beyond 1 Gbps. With this 
	increased growth for bandwidth comes larger system integration complexity and higher energy consumption per packet. Low power design is therefore 
	a major design criterion alongside the standards' throughput requirement as both will determine the quality of service and cost.
</p>

<p class="projectText">
	Error correction plays a major role in communication and storage systems to increase the transmission reliability and achieve a better error correction
 	performance with less signal power. The objective of this project is to develop novel decoding algorithms, architectures and circuits for near Shannon 
	capacity for low-density parity-check (LDPC) decoders. 
</p>

<?php include '../footer.inc.php';?>
