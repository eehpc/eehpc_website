<div class="push" style="height:20px !important"></div>
</div>
<div class="push"></div>
</div>
<div class="footer">
	<div id="footerArea">
		
			<div id="footerInner">
				<div id="footerLeft">
				<a id="footerRef" href="http://www.umbc.edu"></a>
				<div id="footerText" >Energy Efficient High Performance Computing Lab</div>
				</div>
				
				<p class="footerInfo">
					Department of Computer Science and Electrical Engineering<br />
					University of Maryland, Baltimore County <br />
					1000 Hilltop Circle<br />
					Baltimore, MD 21250 USA<br />
					410-455-1000
				</p>
			</div>
	
	</div>
</body>
</html>