<!-- This element is the master element holding within it all the document info -->

<!-- <div id="page"> -->
	<!-- A little bit of hacky code for IE color and rgba -->
	<!--[if IE]>
   
   <style type="text/css">

   .navRef { 
       color:rgb(255,255,255) !important;
    } 

    </style>

<![endif]-->
	<div class="wrapper">
		<div id="navBar">
			<div id="navInner">
				<div id="navLogo"></div>
				<div id="navName">Energy Efficient High Performance Computing Lab</div>
				<div id="tabBar">
					<a href="index.php" class="navRef firstRef">Home</a>
					<a href="people.php" class="navRef">Team</a>
					<a href="research.php" class="navRef">Research</a>
					<a href="publications.php" class="navRef">Publications</a>
					<a href="demos.php" class="navRef">Demos</a>
					<!-- <a href="gallery.php" class="navRef">Gallery</a> -->
					<!--a href="links.php" class="navRef">Links</a-->
					<a href="https://github.com/UMBC-EEHPC" class="navRef">GitHub</a>
					<a href="sponsors.php" class="navRef">Sponsors</a>
					
				</div>
			</div>
		</div>	
		<div id="pageArea">	
