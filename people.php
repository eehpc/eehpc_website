<?php include 'header.inc.php'; ?>

<title>Team@EEHPC</title>
<div id="headerArea">
<!--	<h1>EEHPC Lab</h1> -->
</div>
<!--[if IE 7]>
<style type="text/css">
	.outer {
	height: 154px; overflow: hidden;width:166px;float: left;position: relative;
}
.outer[class] {
	display: table; position: static;
	}

.middle {
	position:absolute; top: 100% !important;
	width:166px;
} /* for adaptive (quirk) explorer only*/
		
.middle[class] {
	display: table-cell; vertical-align: middle; width: 100%; position: static;
}
		
.inner {
	position: relative; top: 0% !important;
}
.peopleArea{
	padding-bottom:10px;
}
</style>
<![endif]-->
<!-- Slideshow Area -->
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script src="jquery.slides.min.js"></script>

  <script>
    $(function(){
      $("#slides").slidesjs({
        width: 940,
        height: 580,
        play: {
	      active: true,
	        // [boolean] Generate the play and stop buttons.
	        // You cannot use your own buttons. Sorry.
	      effect: "slide",
	        // [string] Can be either "slide" or "fade".
	      interval: 5000,
	        // [number] Time spent on each slide in milliseconds.
	      auto: true,
	        // [boolean] Start playing the slideshow on load.
	      swap: true,
	        // [boolean] show/hide stop and play buttons
	      pauseOnHover: false,
	        // [boolean] pause a playing slideshow on hover
	      restartDelay: 2500
	        // [number] restart delay on inactive slideshow
	    },
      });
    });
</script>
<div id="slides">
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/IMG_2608.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
		
	</div>
	
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/group.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
		
	</div>
	
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/Tinoosh17-6989-v2.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
		
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/bestpaper.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
		
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/2015-05-14.jpg');"></div>
		<!-- <div class="caption">this is a caption</div> -->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/DSC_8813.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->		
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/tahmid.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
		
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/2014-group.png');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
		
	</div>

<!--
    <iframe src="https://www.youtube.com/embed/tcdVC4e6EV4?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
-->
</div>

<!-- End slideshow area -->
<div  class="peopleArea">
	<div class="peopleTitle">Faculty</div>
	<div style="margin-bottom: 10px;height:200px" class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/tinoosh.jpg" />
			<a href="http://www.csee.umbc.edu/~tinoosh/" class="personRef">Tinoosh Mohsenin</a>
		</div>
	</div>
</div>
<!--div class="peopleArea">
	<div style="width: 220px" class="peopleTitle">Postdoctoral</div>
	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/saimik.jpg" />
			<a class="personRef" href="https://www.linkedin.com/in/siamak-aram-1a748625/" >Siamak Aram</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">Postdoctoral Research Associate</div>
					<li class="credentialInfo"><b>-</b>Machine Learning, Computational Neuroscience, EEG data analysis for brain functions and activities.</li>
				</div>
			</div>
		</div>
	

	</div>
</div-->
<div class="peopleArea">
	<div style="width: 220px" class="peopleTitle">PhD Students</div>
	<!-- To add anoter person copy from here -->
<!--	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="Images/people/jordan.png" />
			<a class="personRef">Jordan Bisasky</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">PhD Student</div>
					<li class="credentialInfo"><b>-</b> Process Architecture Design</li>
					<li class="credentialInfo"><b>-</b> Application mapping on manycore</li>
					<li class="credentialInfo"><b>-</b> Physical layout design</li>
				</div>
			</div>
		</div>
		
	</div> -->
	<!-- To here -->
	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Mozhgan_Navardi.jpg" />
			<a class="personRef">Mozhgan Navardi</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">PhD Student</div>
					<li class="credentialInfo"><b>-</b> Machine Learning </li>
					<li class="credentialInfo"><b>-</b> Energy Efficient Design </li>
				</div>
			</div>
		</div>		
	</div>
	
	<!--div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/astima.png" />
			<a class="personRef">Asmita Korde</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">PhD Student</div>
					<li class="credentialInfo"><b>-</b> Compressive Sensing</li>
				</div>
			</div>
		</div>
	</div--> 
	<!--div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Colin.png" />
			<a class="personRef">Colin Shea</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">PhD Student</div>
					<li class="credentialInfo"><b>-</b> High performance computing algorithms and hardware </li>
					<li class="credentialInfo"><b>-</b> Parallel Computing for GPU </li>
				</div>
			</div>
		</div>		
	</div-->
	<!--div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/morteza.JPG" />
			<a class="personRef">Morteza Hosseini</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">PhD Student</div>
					<li class="credentialInfo"><b>-</b>Machine Learning</li>
					<li class="credentialInfo"><b>-</b>Markov Chain Monte Carlo</li>
					<li class="credentialInfo"><b>-</b> ASIC and FPGA design</li>
				</div>
			</div>
		</div>	
		
	</div-->
	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Bharat.jpg" />
			<a class="personRef" href="https://www.csee.umbc.edu/~bhp1/">Bharat Prakash</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">PHD Student</div>
					<li class="credentialInfo"><b>-</b>Machine Learning, Reinforcement Learning</li>
				</div>
			</div>
		</div>		
	</div>


	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/hasib1.jpg" />
			<a class="personRef">Hasib-Al Rashid</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">PhD Student</div>
					<li class="credentialInfo"><b>-</b> Machine Learning</li>
					<li class="credentialInfo"><b>-</b> Deep Learning, Recurrent Neural Networks </li>
					<li class="credentialInfo"><b>-</b> ASIC and FPGA design</li>
				</div>
			</div>
		</div>		
	</div>
	
	<!--div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Aidin.jpg" />
			<a class="personRef">Aidin Shiri</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">PhD Student</div>
					<li class="credentialInfo"><b>-</b> Machine Learning</li>
					
					<li class="credentialInfo"><b>-</b> FPGA design</li>
				</div>
			</div>
		</div>		
	</div-->
	
	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Arnab.jpg" />
			<a class="personRef">Arnab Neelim Mazumder</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">PhD Student</div>
					<li class="credentialInfo"><b>-</b> Machine Learning and Signal Processing</li>
					
					<li class="credentialInfo"><b>-</b> FPGA design</li>
				</div>
			</div>
		</div>		
	</div>

	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Uttej.jpg" />
			<a class="personRef">Uttej Kallakuri</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">PhD Student</div>
					<li class="credentialInfo"><b>-</b> FPGA and ASIC Design</li>
					
					<li class="credentialInfo"><b>-</b> Machine Learning and Deep Learning</li>
				</div>
			</div>
		</div>		
	</div>


	
</div>
<div class="peopleArea">
	<div style="width: 280px" class="peopleTitle">Masters Students</div>
	
	<!--div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Griffin.jpg" />
			<a class="personRef" >Griffin Bonner</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">MS Student</div>
					<li class="credentialInfo"><b>-</b></li>
				</div>
			</div>
		</div>		
	</div-->
	
	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Malcolm_Thorpe.jpg" />
			<a class="personRef" >Malcolm Thorpe</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">MS Student</div>
					<li class="credentialInfo"><b>-</b></li>
				</div>
			</div>
		</div>		
	</div>
	
	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Tejaswini_Manjunath.jpg" />
			<a class="personRef" >Tejaswini Manjunath</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">MS Student</div>
					<li class="credentialInfo"><b>-</b>Deep Learning, Reinforcement Learning</li>
				</div>
			</div>
		</div>		
	</div>
	
	
	<!--div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Haoran.png" />
			<a class="personRef" >Haoran Ren</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">MS Student</div>
					<li class="credentialInfo"><b>-</b>Deep Learning, Reinforcement Learning, Computer Vision</li>
				</div>
			</div>
		</div>		
	</div-->

	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/PrakharDixit.jpg" />
			<a class="personRef" >Prakhar Dixit</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">MS Student</div>
					<li class="credentialInfo"><b>-</b></li>
				</div>
			</div>
		</div>		
	</div>

</div>

<div class="peopleArea">
	<div style="width: 280px" class="peopleTitle">Undergraduate Students</div>
	
	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Kamran_Ataran_Rezai_strech_fix_try2.jpg" />
			<a class="personRef" href="">Kamran_Ataran_Rezai</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">BS Student</div>
					<li class="credentialInfo"><b>-</b>Reinforcement learning</li>
				</div>
			</div>
		</div>		
	</div>
	
	<!--div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/yeerfan.jpg" />
			<a class="personRef" href="">Yeerfan Tuerdi</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">BS Student</div>
					<li class="credentialInfo"><b>-</b>Manycore Compiler</li>
				</div>
			</div>
		</div>
	</div-->
	
	<!--div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/mark_horton.jpg" />
			<a class="personRef" href="">Mark Horton</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">BS Student</div>
					<li class="credentialInfo"><b>-</b>Model-based reinforcement learning</li>
				</div>
			</div>
		</div>		
	</div-->

	<!--div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/Kelsey_strech_fix.jpg" />
			<a class="personRef" href="">Kelsey</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">BS Student</div>
					<li class="credentialInfo"><b>-</b></li>
				</div>
			</div>
		</div>		
	</div-->

	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/DavidLedbetter.jpg" />
			<a class="personRef" href="">David Ledbetter</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">BS Student</div>
					<li class="credentialInfo"><b>-</b></li>
				</div>
			</div>
		</div>		
	</div>

	<div class="personArea">
		<div class="nameArea">
			<img class="personImg" src="src/images/people/EdwardHumes.jpeg" />
			<a class="personRef" >Edward Humes</a>
		</div>
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="graduateLevel">BS Student</div>
					<li class="credentialInfo"><b>-</b></li>
				</div>
			</div>
		</div>		
	</div>

</div>


<div class="peopleArea">
	<div class="peopleTitle">Alumni</div>
	
	<table>
		<tbody>
			<tr> <!--vertical space -->
				<td>&nbsp;</td>
			</tr>
			<tr align = "left" valign = "top" bgcolor="#D3D3D3">
				<td columnspan = "6">
					<p style="margin-left:10px;margin-top:5px;margin-right:0px;margin-bottom:5px;">Postdoctoral</p>
				</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src= "src/images/people/Siamak.jpg" />
						<a class="personRef" href="https://www.linkedin.com/in/siamak-aram-1a748625/" >Dr. Siamak Aram</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<!---div class="graduateLevel">Postdoctoral Research Associate</div>
						<li class="credentialInfo"><b>-</b>Machine Learning, Computational Neuroscience, EEG data analysis for brain functions and activities.</li--->
						<p> <font size="3">Postdoctoral Research Associate. CE, Dec. 2017 <br>Associate Professor at Harrisburg University of Science and Technology <br></font></p>
						<li class="credentialInfo"><b>-</b> Machine Learning, Computational Neuroscience, EEG data analysis for brain functions and activities </li>
					</div>
				</td>
		</tbody>
	</table>
	
	<table>
		<tbody>
			<tr> <!--vertical space -->
				<td>&nbsp;</td>
			</tr>
			<tr align = "left" valign = "top" bgcolor="#D3D3D3">
				<td columnspan = "6">
					<p style="margin-left:10px;margin-top:5px;margin-right:0px;margin-bottom:5px;">Ph.D.</p>
				</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/morteza.JPG" />
						<a href="https://www.linkedin.com/in/xmhosseini/" class="personRef">Dr. Morteza Hosseini</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">Ph.D. CE, May 2022 <br>Senior Engineer at Qualcomm<br>Qualcomm</font></p>
						<li class="credentialInfo"><b>-</b> Machine Learning </li>
						<li class="credentialInfo"><b>-</b> Markov Chain Monte Carlo</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/astima.png" />
						<a href="" class="personRef">Dr. Asmita Korde</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">Ph.D. CE, May 2022 <br>Senior Engineer at NASA<br><a href=><br></a></font></p>
						<li class="credentialInfo"><b></b></li>
						<li class="credentialInfo"><b></b></li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Colin.png" />
						<a href="https://www.linkedin.com/in/colinwshea/" class="personRef">Dr. Colin Shea</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">Ph.D. CE, Nov. 2021 <br> Technical Research Fellow at Boeing <br>Boeing </font></p>
						<li class="credentialInfo"><b>-</b> High performance computing algorithms and hardware </li>
						<li class="credentialInfo"><b>-</b> Parallel Computing for GPU</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Ali.jpg" />
						<a href="https://www.linkedin.com/in/ajafari1/" class="personRef">Dr. Ali Jafari</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">Ph.D. CE, Dec. 2017 <br>Research Scientist<br><a href="http://intel.com/">Intel<br></a></font></p>
						<li class="credentialInfo"><b>-</b> Compressive sensing </li>
						<li class="credentialInfo"><b>-</b> Digital /Analog ASIC design for low power biomedical applications </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Amey.jpeg" />
						<a href="https://www.linkedin.com/in/ameyk/" class="personRef">Dr. Amey Kulkarni</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">Ph.D. CE, Feb. 2017 <br>Senior Embedded Systems Engineer <br><a href="https://www.nvidia.com/en-us/">Nvidia  </a></font></p>
						<li class="credentialInfo"><b>-</b> Computer Vision </li>
						<li class="credentialInfo"><b>-</b> Compressive Sensing</li>
						<li class="credentialInfo"><b>-</b> Hardware Security</li>
						<li class="credentialInfo"><b>-</b> Low Power FPGA and ASIC Design</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>

				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/adam.png" />
						<a class="personRef">Dr. Adam Page</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">Ph.D. CE, Dec. 2016 <br>Principal Engineer, Machine Learning AI<br>Ambiq</font></p>
						<li class="credentialInfo"><b>-</b> Deep learning </li>
						<li class="credentialInfo"><b>-</b> Embedded Big Data Optimization </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<!--MS students-->
			<tr align = "left" valign = "top" bgcolor="#D3D3D3">
				<td columnspan = "6">
					<p style="margin-left:10px;margin-top:5px;margin-right:0px;margin-bottom:5px;">MS</p>
				</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>

			<tr>
			<td valign="top">
				<div class="nameArea">
					<img class="personImg" src="src/images/people/Haoran.png" />
					<a href="https://www.linkedin.com/in/vandanachandrareddy/" class="personRef">Haoran Ren</a>			
				</div>
			</td>
			<td valign="top">
				<div style="width:280px;text-align:left;">
					<p> <font size="3">MS. CE, August 2021<br><a href="">Deep Learning Amazon</a><br></font></p>
					<li class="credentialInfo"><b>-</b> Deep Learning, Reinforcement Learning, Computer Vision </li>
				</div>
			</td>
			<td style="width:60px">&nbsp;</td>
			<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Griffin.jpg" />
						<a href="" class="personRef">Griffin Bonner</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, May 2022<br><a href=""></a><br></font></p>
						<li class="credentialInfo"><b></b></li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>

			<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Vandana.jpg" />
						<a href="https://www.linkedin.com/in/vandanachandrareddy/" class="personRef">Vandana Chandrareddy</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, August 2021<br><a href="">R&D Engineer Nvidia</a><br></font></p>
						<li class="credentialInfo"><b>-</b> Machine Learning and Data Science, FPGA and ASIC design </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Niteesh.jpg" />
						<a href="https://www.linkedin.com/in/nitheeshkm/" class="personRef">Nitheesh Kumar Manjunath</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, December 2020<br><a href="">FPGA Design Engineer Broadcast Sports International</a><br></font></p>
						<li class="credentialInfo"><b>-</b> Deep Learning, Reinforcement Learning, ASIC and FPGA Design</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Uttej.jpg" />
						<a href="https://www.linkedin.com/in/uttejkaallakuri/" class="personRef">Uttej Kallakuri</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, December 2019<br><a href=""></a><br></font></p>
						<li class="credentialInfo"><b>-</b> Hardware Accelarator Design for Sparse Matrix Vector Multiplication </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Hiren_Paneliya.jpg" />
						<a class="personRef" href="https://www.linkedin.com/in/hiren-paneliya-193192100/">Hirenkumar Paneliya</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, June 2020<br><a href="https://www.marvell.com/">Marvell Technology Group</a><br></font></p>
						<li class="credentialInfo"><b>-</b> CSCMAC - Cyclic Sparsely Connected Neural Network Manycore Accelerator </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			
			
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Mohit.jpg" />
						<a href="https://www.linkedin.com/in/mohit-khatwani-a78207104/" class="personRef">Mohit Khatwani</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CS, August 2019<br><a href="https://www.transunion.com">TransUnion</a><br></font></p>
						<li class="credentialInfo"><b>-</b>Efficient Artifact Identification on Multi-Channel EEG Signal </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Nathan.png" />
						<a class="personRef" href="https://www.linkedin.com/in/varun-sivasubramanian-69377267/">Nathanael Buswell</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, December 2017<br><a href="http://www.northopgrumman.com/">Northrop Grumman</a><br></font></p>
						<li class="credentialInfo"><b>-</b>Tongue drive system, FPGA implementation </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/adwaya1.png" />
						<a href="https://www.linkedin.com/in/adwaya-kulkarni" class="personRef">Adwaya Kulkarni</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, August 2017<br><a href="http://intel.com/">Intel</a><br></font></p>
						<li class="credentialInfo"><b>-</b> Many-Core Architecture, ASIC Design and Verification </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/varun.jpeg" />
						<a class="personRef" href="https://www.linkedin.com/in/varun-sivasubramanian-69377267/">Varun Sivasubramanian</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, May 2018<br>Application Support Engineer<br><a href="http://equifax.com/">Equifax</a><br></font></p>
						<li class="credentialInfo"><b>-</b>Stress detection, Manycrore, Machine Learning</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/lahir.jpeg" />
						<a href="https://in.linkedin.com/in/lahir-marni-b4bb9439" class="personRef">Lahir Marni</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, May 2018<br><a href="http://vectorworks.net/">Vector Works</a><br></font></p>
						<li class="credentialInfo"><b>-</b> Parallel and Distributed Processing, Machine Learning, Markov Chain Monte Carlo</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/chetan.JPG" />
						<a class="personRef">Chetan Sai Kumar Thalisetty</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, May 2018<br>Software Development Engineer<br><a href="http://amazon.com/">Amazon</a><br></font></p>
						<li class="credentialInfo"><b>-</b>Deep Neural Networks</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Abhilash.jpg" />
						<a class="personRef" >Abhilash Puranik</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, August 2017<br> Embedded Software Engineer<br><a href="https://www.kpit.com/">KPIT Infosystems</a><br></font></p>
						<li class="credentialInfo"><b>-</b> Manycore, Parallel computing for GPU </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Sri Harsha1.jpg" />
						<a class="personRef">Sri Harsha Konuru</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, July 2017<br></font></p>
						<li class="credentialInfo"><b>-</b> High performance computing algorithms, Parallel computing for GPU </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/tahmid_abtahi.jpg" />
						<a class="personRef"href="https://www.linkedin.com/pub/tahmid-abtahi/79/20a/1a4">Tahmid Abtahi</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, July 2017<br>Embedded hardware engineer<br><a href="http://www.senseonics.com/">Senseonics</a><br></font></p>
						<li class="credentialInfo"><b>-</b> Low power manycore design </li>
						<li class="credentialInfo"><b>-</b> Compressive Sensing </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/nasrin2.jpg" />
						<a class="personRef" >Nasrin Attaran</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, May 2017<br><br></font></p>
						<li class="credentialInfo"><b>-</b> Low power algorithms and hardware </li>
						<li class="credentialInfo"><b>-</b> Domain specific manycore </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Emily Smith.jpg" />
                        <a class="personRef">Emily Smith</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, Dec. 2015<br>Hardware Engineer</font></p>
						<li class="credentialInfo"><b>-</b> Low power hardware for biomedical applications</li>
                        <li class="credentialInfo"><b>-</b> Domain specific processor design</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Chris Sagedy.jpg" />
                        <a class="personRef">Chris Sagedy</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, Dec. 2015<br>Hardware Engineer</font></p>
						<li class="credentialInfo"><b>-</b> Low power hardware for biomedical applications</li>
                        <li class="credentialInfo"><b>-</b> Domain specific processor design</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/sina.png" />
						<a class="personRef" href="http://www.linkedin.com/in/sinaviseh/">Sina Viseh</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, Aug. 2014<br>Software Engineer <br>IfOnly</font></p>
						<li class="credentialInfo"><b>-</b> FPGA and ASICs for wearable biomedical devices</li>
						<li class="credentialInfo"><b>-</b> Efficient machine learning</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/darin.png" />
						<a class="personRef">James Chandler</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">MS. CE, May 2012<br>Hardware Engineer</font></p>
						<li class="credentialInfo"><b>-</b> On-chip interconnects for manycore chips</li>
						<li class="credentialInfo"><b>-</b> VLSI and FPGA hardware design</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			
			
			<!--BS Students-->
			<tr align = "left" valign = "top" bgcolor="#D3D3D3">
				<td columnspan = "6">
					<p style="margin-left:10px;margin-top:5px;margin-right:0px;margin-bottom:5px;">BS</p>
				</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/mark_horton.jpg" />
						<a class="personRef" href="">Mark Horton</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">BS. CE, May 2019<br><br></font></p>
						<li class="credentialInfo"><b></b></li>
					</div>
				</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Kelsey_strech_fix.jpg" />
						<a class="personRef" href="">Kelsey</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">BS. CE, May 2017<br><br></font></p>
						<li class="credentialInfo"><b></b></li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/yeerfan.jpg" />
						<a class="personRef">Yeerfan Tuerdi</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">BS. CE, May 2016<br><br></font></p>
						<li class="credentialInfo"><b>-</b>Manycore Compiler</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Elise.png" />
						<a class="personRef" href="https://www.linkedin.com/in/elise-donkor-97a937105">Elise Donkor</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">BS. CE, May 2016<br>Microsoft research<br></font></p>
						<li class="credentialInfo"><b>-</b>  ECG arrythmia detection </li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Vignesh.jpg" />
						<a class="personRef">Vignesh Dhanasekaran</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">BS. CE, May 2015<br> Northrop Grumman<br></font></p>
						<li class="credentialInfo"><b>-</b> Low power algorithms for manycore</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Kamal.png"/>
						<a class="personRef">Kamal Broomes</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">BS. CE, May 2014<br></font></p>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/Alec.png"/>
						<a class="personRef">Alec Pulianas</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">BS. CE, May 2014<br></font></p>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/paul.png" />
						<a class="personRef">Paul Boudra</a>			
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">BS. CE, May 2013<br></font></p>
						<li class="credentialInfo"><b>-</b> FPGA</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
				<td valign="top">
					<div class="nameArea">
						<img class="personImg" src="src/images/people/julian.png" />
						<a class="personRef">Julian Feild</a>
					</div>
				</td>
				<td valign="top">
					<div style="width:280px;text-align:left;">
						<p> <font size="3">BS. CE, May 2013<br></font></p>
						<li class="credentialInfo"><b>-</b> Application Mapping</li>
						<li class="credentialInfo"><b>-</b> Ultrasound Spectral Doppler</li>
					</div>
				</td>
				<td style="width:60px">&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			<tr> 
				<td>&nbsp;</td>
			</tr>
			
		</tbody>
	</table>
</div>
<?php include 'footer.inc.php' ?>
