# README
This is the official repository for the EEHPC website. The objective was to separate the webdesign content from the lab sources. For those attempting to add additional content, this will mostly take place in src folder.

## Updates

### Publications
The publication sources are stored in **publications/recent.html** and **publications/prior.html**

### Home Slideshow
The Home slideshow source is stored in **index.php** at **`<div id="slides">`**. The image files are found within **src/images/** folder.

### Team
The Team slideshow source is stored in **people.php** at **`<div id="slides">`**. The image files are found within **src/images/team/** folder.

The Team picture web code is stored in **people.php**. Templates are given within.

### Research
The research material is split into 3 sections. **research/research.html** provides the summary for each of the research project. Each research project requires an icon to be stored in **src/images/research_icon/**. The project's separate writeup is stored in **projects/**. Examples are provided.

## Update

Updating the website should be done as follows:

1. **git clone** _this-repository_ onto you local machine
2. Make any necessary changes
3. Make sure to commit and push changes onto master 

In order for changes to be reflected on actual website, run the following script on the EEHPC Server ( @linuxserver1.cs.umbc.edu ):

- 'sh /data/eehpc1/eehpc_website_update/update_website.sh'

All this is doing is downloading the source from master, unzipping, and copying into **/web/sites/eehpc.csee.umbc.edu/htdocs/**.