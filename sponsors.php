<?php include 'header.inc.php'; ?>

<title>Sponsors@EEHPC</title>
<div id="headerArea">
	<h1>Sponsors</h1>
	<h3>We are grateful for the support of our sponsors:</h3>
</div>
<table style="height: 422px; border-color: #ffffff; background-color: #ffffff;" width="1000">
	<tbody>
		<tr>
			<td style="width=150px;"><img src="src/images/sponsors/NSF.png" alt="NSF" width="135" height="135"></td>
			<td style="width=200px;"><img src="src/images/sponsors/ARL.png" alt="ARL" width="200" height="70"></td>
			<td style="width=250px;"><img src="src/images/sponsors/Xilinx.jpg" alt="Xilinx" width="250" height="70"></td>
			<td style="width=300px;"><img src="src/images/sponsors/LockheedMartin.png" alt="LockheedMartin" width="300" height="70"></td>
			
		</tr>
		<tr>
			<td><img src="src/images/sponsors/DCSCorp.jpg" alt="DCSCorp" width="135" height="135"></td>
			<td><img src="src/images/sponsors/Nvidia.png" alt="Nvidia" width="200" height="50"></td>
			<td><img src="src/images/sponsors/Boeing.jpg" alt="Boeing" width="250" height="70"></td>
			<td><img src="src/images/sponsors/UMSoM.png" alt="UMSoM" width="300" height="70"></td>
		</tr>
		<tr>
			<td><img src="src/images/sponsors/NASA.jpeg" alt="NASA" width="135" height="135"></td>
			<td><img src="src/images/sponsors/lps_logo_sm.gif" alt="LPS" width="200" height="50"></td>
			<td><img src="src/images/sponsors/DARPA_Logo_2010.png" alt="DARPA" width="250" height="70"></td>
		</tr>
	</tbody>
</table>

<?php include 'footer.inc.php' ?>