<?php include 'header.inc.php'; ?>

<head>
     <link rel="icon" type="image/png" href="Images/footer/mini_icon.png" />
</head>
<title>Research@EEHPC</title>
<div id="headerArea">
	<h2>Current Research</h2>
</div>
<div class="projectArea">
	<?php include 'research/research.html'; ?>
</div>
<?php include 'footer.inc.php' ?>
