<?php include 'header.inc.php'; ?>

<head>
<title>EEHPC Lab </title>	 
</head>
<div id="headerArea">
	<h1></h1>
</div>
<script src="jquery-latest.min.js"></script>
  <script src="jquery.slides.min.js"></script>

  <script>
    $(function(){
      $("#slides").slidesjs({
        width: 600,
        height: 375,
        play: {
	      active: true,
	        // [boolean] Generate the play and stop buttons.
	        // You cannot use your own buttons. Sorry.
	      effect: "slide",
	        // [string] Can be either "slide" or "fade".
	      interval: 5000,
	        // [number] Time spent on each slide in milliseconds.
	      auto: true,
	        // [boolean] Start playing the slideshow on load.
	      swap: true,
	        // [boolean] show/hide stop and play buttons
	      pauseOnHover: true,
	        // [boolean] pause a playing slideshow on hover
	      restartDelay: 2500
	        // [number] restart delay on inactive slideshow
	    },
      });
    });
</script>
<div id="slides">
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/CARDS-Research-22-3693.jpg'); background-size: 80%;"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/IMG_8787-2.jpg'); background-size: 80%;"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<!--div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/IMG_4030.jpg'); background-size: 80%;"></div>
		<<div class="caption"> Initial prototype of the TDS </div>>
	</div-->
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/20220906_140940.jpg'); background-size: 80%;"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/IMG20220427161425-2.jpg'); background-size: 80%;"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/IMG_1683-2.jpg'); background-size: 80%;"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	---
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/Group_pic_2020.jpg'); background-size: 80%;"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<!--div>
		<div class="slidesjs-image" style="background-image: url('src/images/gallery/20211005_154910.jpg');"></div>
		< <div class="caption"> Initial prototype of the TDS </div>>
	</div-->
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/gallery/20210116_114223.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/gallery/image_6483441.JPG'); background-size: 80%;"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/gallery/2BF0C635-01BE-42DE-98C0-180BAADAA170_1_105_c.jpeg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/gallery/Elise Donkor.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/gallery/20210810_105746.jpg'); background-size: 80%;"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/gallery/Tinoosh17-6971.jpg'); background-size: 80%;"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/DSC_8726.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/DSC_8750.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/DSC_8763.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/team/DSC_8770.jpg');"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	<div>
		<div class="slidesjs-image" style="background-image: url('src/images/research/Alidemo.jpg'); background-size: 80%;"></div>
		<!-- <div class="caption"> Initial prototype of the TDS </div>	-->
	</div>
	
	
	
<!--
    <iframe src="https://www.youtube.com/embed/tcdVC4e6EV4?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
-->
</div>

<div id="overviewText">
	<h1>Overview</h1>

	<p>
		Our research focus is designing highly accurate and high-performance computing methods for emerging fields in artificial intelligence, robotics and autonomous navigation, smart health monitoring, and Cyber Physical Systems. Our team has developed efficient computing methods through cross-layer design approaches in algorithms, architecture, hardware and system integration which result in tiny and energy efficient wearable/mobile computing devices. This can revolutionize several fields including healthcare, transportation, autonomous navigation, security, surveillance, and public utilities.
	</p>
<!-- 
	<p>
		Energy Efficient High Performance Computing Group is involved with the design and implementation of various high speed and high performance systems which are highly energy efficient.
	</p>
	<p>
		The research spans across multiple levels of abstraction ranging from innovative new process technologies and circuit styles to architectures, algorithms, and software technologies.
	</p>
 -->	 
	<div class="push" style="height:0 !important"> </div>
</div>

<div id="overviewText">
	<h1>News Articles</h1>

	<p>
		<a href="https://www.cbsnews.com/baltimore/news/cutting-edge-umbc-research-uses-artificial-intelligence-and-robots-to-assist-national-security/">  EEHPC Lab Featured in Baltimore CBS News WJZ TV in AI Series, “Cutting-edge UMBC research uses artificial intelligence and robots to assist national security, February 16 2023. </a><br>
	</p>
	
	<p>
		<a href="https://umbc.edu/stories/umbc-experts-on-promises-and-pitfalls-of-artificial-intelligence/">  EEHPC Lab Featured in UMBC News Story: “UMBC experts guide TV viewers through the promises and pitfalls of artificial intelligence, February 2023. </a><br>
	</p>
	
	<p>
		<a href="https://twitter.com/HellgrenWJZ/status/1626380527207870466?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1626380527207870466%7Ctwgr%5E07b9396f8224ea844119111d24e3a8d98d4e23d6%7Ctwcon%5Es1_c10&ref_url=https%3A%2F%2Fwww.cbsnews.com%2Fbaltimore%2Fnews%2Fcutting-edge-umbc-research-uses-artificial-intelligence-and-robots-to-assist-national-security%2F">  EEHPC Lab Featured in Twitter by CBS News Baltimore WJZ Investigator Mike Hellgren  for discussing the use of tiny drones for disaster response. </a><br>
	</p>
	
	<p>
		<a href="https://my3.my.umbc.edu/groups/csee/posts/127509">  EEHPC Lab Featured in UMBC News Story for First-placed Award ARL Sponsored Hackathon and Presentation in Human-Guided System Adaptation (HSA) Science Challenge and Hackathon Week, Columbia University, NY, August,2022. </a><br>
	</p>
	
	<p>
		<a href="https://news.umbc.edu/umbcs-tinoosh-mohsenin-develops-covid-matter-framework-to-determine-severity-of-respiratory-disease/">  EEHPC Lab Featured UMBC News Story: “UMBC’s Tinoosh Mohsenin develops COVID-Matter framework to determine severity of respiratory disease, 2022. </a><br>
	</p>
	
	<p>
		<a href="https://news.umbc.edu/umbc-celebrates-u-s-news-best-grad-school-rankings-in-engineering-public-affairs/">  EEHPC Lab Featured in UMBC News: “UMBC celebrates U.S. News Best Grad School rankings in engineering, public affairs, 2022. </a><br>
	</p>
	
	<p>
		<a href="https://news.umbc.edu/umbc-to-partner-with-umd-army-research-lab-to-advance-ai-and-autonomy-through-68m-collaboration/">  Prof. Mohsenin Featured in UMBC News: “UMBC to partner with UMD, Army Research Lab to advance AI and autonomy through $68M collaboration, 2022. </a><br>
	</p>
	
	<p>
		<a href="https://www.csee.umbc.edu/2017/04/umbc-prof-tinoosh-mohsenin-receives-nsf-career-award-deep-learning-technologies/">  Prof. Mohsenin featured in UMBC News, 2017. </a><br>
	</p>
	
	<p>
		<a href="http://magazine.umbc.edu/staking-our-claim/">  Prof. Mohsenin featured UMBC Magazine, “UMBC Researchers Explore the New Great Frontier – The Brain”. </a><br>
	</p>
<!-- 
	<p>
		Energy Efficient High Performance Computing Group is involved with the design and implementation of various high speed and high performance systems which are highly energy efficient.
	</p>
	<p>
		The research spans across multiple levels of abstraction ranging from innovative new process technologies and circuit styles to architectures, algorithms, and software technologies.
	</p>
 -->	 
	<div class="push" style="height:0 !important"> </div>
</div>
<?php include 'footer.inc.php' ?>
