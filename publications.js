//Change this to the current year 
var currentYear="2014";


//start looking here
function setYear(ele){
	var caller=ele.id;
	//Checks to make sure this year isn't already selected
	if(currentYear!=caller){
		document.getElementById("publicationInfo").innerHTML= document.getElementById("year"+caller).innerHTML;
		document.getElementById(caller).setAttribute("class","pubYearButOn");
		document.getElementById(currentYear).setAttribute("class","pubYearBut");
		currentYear= caller;
		
	}
	
}
